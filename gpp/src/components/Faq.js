import React from 'react';
import {Container} from 'semantic-ui-react';


const Faq = () => (
  <Container>
    <h1>FAQ</h1>
    <ul>
      <li><a href="#1">Donec sed odio dui.</a></li>
      <li><a href="#2">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</a></li>
      <li><a href="#3">Donec id elit non mi porta gravida at eget metus.</a></li>
      <li><a href="#4">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</a></li>
    </ul>
    <h3 id="1">Donec sed odio dui.</h3>
    <p>
      Vestibulum id ligula porta felis euismod semper. Maecenas faucibus mollis interdum. Cum sociis natoque penatibus
      et magnis dis parturient montes, nascetur ridiculus mus. Nulla vitae elit libero, a pharetra augue. Aenean eu leo
      quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Maecenas sed diam eget risus varius blandit sit
      amet non magna.
    </p>
    <p>
      Cras mattis consectetur purus sit amet fermentum. Sed posuere consectetur est at lobortis. Curabitur blandit
      tempus porttitor. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Vestibulum id
      ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut
      fermentum massa justo sit amet risus. Nulla vitae elit libero, a pharetra augue.
    </p>
    <p>
      Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed posuere consectetur est
      at lobortis. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Aenean eu leo quam. Pellentesque ornare
      sem lacinia quam venenatis vestibulum. Curabitur blandit tempus porttitor. Aenean eu leo quam. Pellentesque ornare
      sem lacinia quam venenatis vestibulum. Donec id elit non mi porta gravida at eget metus.
    </p>
    <h3 id="2">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</h3>
    <p>
      Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Donec id elit non mi porta gravida
      at eget metus. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Nullam quis risus eget urna mollis
      ornare vel eu leo. Aenean lacinia bibendum nulla sed consectetur. Nulla vitae elit libero, a pharetra augue.
      Donec ullamcorper nulla non metus auctor fringilla. Maecenas sed diam eget risus varius blandit sit amet non
      magna. Nulla vitae elit libero, a pharetra augue. Aenean eu leo quam. Pellentesque ornare sem lacinia quam
      venenatis vestibulum. Donec id elit non mi porta gravida at eget metus.
    </p>
    <h3 id="3">Donec id elit non mi porta gravida at eget metus.</h3>
    <p>
      Nullam id dolor id nibh ultricies vehicula ut id elit. Nullam id dolor id nibh ultricies vehicula ut id elit.
      Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum. Lorem ipsum dolor sit amet,
      consectetur adipiscing elit. Cras mattis consectetur purus sit amet fermentum.
    </p>
    <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit. Cras justo odio, dapibus ac facilisis in, egestas eget
      quam. Cras mattis consectetur purus sit amet fermentum. Cum sociis natoque penatibus et magnis dis parturient
      montes, nascetur ridiculus mus.
      Etiam porta sem malesuada magna mollis euismod. Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis
      vestibulum. Aenean lacinia bibendum nulla sed consectetur. Sed posuere consectetur est at lobortis. Cras mattis
      consectetur purus sit amet fermentum.
    </p>
    <h3 id="4">Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor.</h3>
    <p>
      Donec ullamcorper nulla non metus auctor fringilla. Cras justo odio, dapibus ac facilisis in, egestas eget quam.
      Vestibulum id ligula porta felis euismod semper. Cras mattis consectetur purus sit amet fermentum. Nulla vitae
      elit libero, a pharetra augue. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus
      mus.
    </p>
    <br/>
    <br/>
  </Container>
);

export default Faq;